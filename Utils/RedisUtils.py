#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/20 9:58
# @Author : Carey
# @File : RedisUtils.py
# @Description

from flask_redis import FlaskRedis

class RedisUtil:

    redis = FlaskRedis()
    def __init__(self):
        pass

    @classmethod
    def inject_app(cls, app):
        cls.redis.init_app( app )

        return cls.redis

    """
    set
    """
    def set(self, key, value):
        self.redis.set(key, value)

    """
    get
    """
    def get(self, key):
        value = self.redis.get(key)
        if value:
            return value.decode()
        else:
            return None



    """
    delete
    """
    def delete(self, key):
        result = self.redis.delete(key)
        return result