#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/15 18:18
# @Author : Carey
# @File : database.py
# @Description

"""
数据库配置

安装 pip install  flask-sqlalchemy
"""

default = 'mysql'

"""
驱动
"""
drivers = {
    'mysql': {
        'host': '127.0.0.1',
        'uname': 'root',
        'password': '123456',
        'port': 3306,
        'db': 'zendesk',
        'charset': 'utf8mb4'
    }
}


if __name__ == '__main__':
    print( drivers[default] )