#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/20 9:13
# @Author : Carey
# @File : cache.py
# @Description

"""
缓存配置
redis
"""

default = 'redis'


"""
驱动
"""
drivers = {
    'redis': {
        'host': '127.0.0.1',
        'port': 3306,
        'password': '',
        'db': 0
    }
}


if __name__ == '__main__':
    print( drivers[default] )
