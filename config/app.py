#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/15 17:50
# @Author : Carey
# @File : bootstrap.py
# @Description


"""
系统配置
    -host   主机
    -port   端口
    -debug  调试模式
    -view   视图位置
    -static 静态资源位置
"""


"""
系统host
"""
HOST = '0.0.0.0'


"""
系统端口
"""
PORT = 8080

"""
调试模式
"""
DEBUG = True


"""
视图文件
"""
TEMPLATE = '../resources/views'

"""
静态资源文件
"""
STATIC = '../resources/assets'







if __name__ == '__main__':
    pass