#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/15 14:06
# @Author : Carey
# @File : bootstrap.py
# @Description
from app.Kernel import Kernel

### 启动
if __name__ == '__main__':

    Kernel.bootstrap()
