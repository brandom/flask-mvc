#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/20 9:29
# @Author : Carey
# @File : Redis.py
# @Description

"""
实例化redis缓存驱动
"""

class Redis:

    def __init__(self, conf):
        """
        初始化
        """
        self.config = conf

        if 'list' == type( conf ):
            self.REDIS_CLUSTER = self.connects()
        else:
            self.REDIS_URL = self.connect()

    def connect(self):
        """
        链接服务
        """
        conn = f"redis://:{self.config['password']}@{self.config['host']}:{self.config['port']}/{self.config['db']}"

        return conn


    def connects(self):
        """
        链接服务
        """
        conns = []
        for item in self.config:
            conns.append( f"redis://:{item['password']}@{item['host']}:{item['port']}/{item['db']}" )

        return conns


if __name__ == '__main__':
    pass