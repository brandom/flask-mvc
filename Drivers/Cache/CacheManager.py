#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/20 9:27
# @Author : Carey
# @File : CacheManager.py
# @Description

from Flask.config import cache
from Flask.Utils.RedisUtils import RedisUtil
from Flask.Drivers.Cache.Redis import Redis
from flask_redis import FlaskRedis

class Cache:
    """
    数据库抽象
    """
    config = cache
    def __init__(self):
        """
        初始化
        """
        self.config = cache

    """
    初始化缓存驱动
    """
    @classmethod
    def init(cls, app):

        if 'redis' == cls.config.default:
            cls.driver = Redis(cls.config.drivers[cls.config.default])

        app.config.from_object(cls.driver)
        redis = FlaskRedis()
        redis.init_app( app )

        return redis


if __name__ == '__main__':
    a = Cache()