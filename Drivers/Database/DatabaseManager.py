#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/18 10:07
# @Author : Carey
# @File : DatabaseManager.py
# @Description
from Flask.config import database
from Flask.Drivers.Database.Mysql import Mysql
from Flask.app.Models.Base import Base

class Database:
    """
    数据库抽象
    """
    config = database
    def __init__(self):
        """
        初始化
        """
        self.config = database

    """
    初始化数据库驱动
    """
    @classmethod
    def init(cls, app ):
        try:
            if 'mysql' == cls.config.default:
                cls.driver = Mysql( cls.config.drivers[cls.config.default] )

        except Exception as e:
            raise Exception( e )

        app.config.from_object( cls.driver )
        Base.inject_app( app )

        return True


if __name__ == '__main__':
    a = Database()