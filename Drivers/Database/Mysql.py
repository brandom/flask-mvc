#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/15 18:22
# @Author : Carey
# @File : Mysql.py
# @Description

"""
sql驱动
"""
class Mysql:


    def __init__(self, conf ):
        """
        初始化
        """
        self.config = conf
        self.SQLALCHEMY_TRACK_MODIFICATIONS = True
        self.SQLALCHEMY_DATABASE_URI = self.connect()
        self.SQLALCHEMY_ECHO = True

    def connect(self):
        """
        链接数据库
        """
        conn = f"mysql+pymysql://{self.config['uname']}:{self.config['password']}@{self.config['host']}:{self.config['port']}/{self.config['db']}"
        if self.config['charset']:
            conn += f"?charset={self.config['charset']}"

        return conn



if __name__ == '__main__':
    pass