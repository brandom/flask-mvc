#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/19 17:18
# @Author : Carey
# @File : Users.py
# @Description

from Flask.app.Models.Base import Base


class Users( Base.db.Model ):
    """
    站点数据表映射
    """
    db = Base.db
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True )
    created_at = db.Column(db.DateTime, nullable=False )
    updated_at = db.Column(db.DateTime, nullable=False )
    name = db.Column(db.String(32),  index = True, nullable=False )
    email = db.Column(db.String(64), unique=True, index = True, nullable=False )
    password = db.Column(db.String(64), unique=True, index = True, nullable=False )
    email_verified_at = db.Column( db.Time )
    email_verified_at = db.Column(db.String(100))

    # 关联关系
    """
    lazy: 指定sqlalchemy数据库什么时候加载数据
       select: 就是访问到属性的时候，就会全部加载该属性的数据
       joined: 对关联的两个表使用联接
       subquery: 与joined类似，但使用子子查询
       dynamic: 不加载记录，但提供加载记录的查询，也就是生成query对象
    """
    sites = db.relationship( "Sites", backref='user', uselist=True, lazy='dynamic' )

    def __repr__(self):
        return '<Users %s>' % self.name