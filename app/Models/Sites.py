#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/18 11:05
# @Author : Carey
# @File : Sites.py
# @Description
from Flask.app.Models.Base import Base


class Sites( Base.db.Model ):
    """
    站点数据表映射
    """
    db = Base.db
    __tablename__ = 'sites'

    id = db.Column( db.Integer, primary_key=True, autoincrement=True )
    created_at = db.Column(db.DateTime, nullable=False )
    updated_at = db.Column(db.DateTime, nullable=False )
    deleted_at = db.Column(db.DateTime, nullable=True )
    name = db.Column(db.String(32),  index = True, nullable=False )
    email = db.Column(db.String(64), unique=True, index = True, nullable=False )
    phone = db.Column(db.String(64), unique=True, index = True, nullable=False )
    remarks = db.Column(db.String(128))

    # 外键关联 db.foreignKey
    uid = db.Column( db.Integer, db.ForeignKey( 'users.id' ), nullable=False )
    # 关联关系
    """
    lazy: 指定sqlalchemy数据库什么时候加载数据
       select: 就是访问到属性的时候，就会全部加载该属性的数据
       joined: 对关联的两个表使用联接
       subquery: 与joined类似，但使用子子查询
       dynamic: 不加载记录，但提供加载记录的查询，也就是生成query对象
    """
    #user = db.relationship( 'Users', backref="sites" )

    def __repr__(self):
        return '<Site %s>' % self.name



if __name__ == '__main__':
    pass
