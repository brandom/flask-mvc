#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/19 16:49
# @Author : Carey
# @File : Base.py
# @Description
from flask_sqlalchemy import SQLAlchemy

class Base:
    db = SQLAlchemy()

    def __init__(self):
        pass

    @classmethod
    def inject_app(cls, app):
        cls.db.init_app( app )

        return cls.db