#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time   : 2024/3/19 18:30
# @Author : Carey
# @File : Kernel.py
# @Description

from flask import Flask
from Flask.Drivers.Database.DatabaseManager import Database
from Flask.app.Controller.dashbord import bp
import Flask.config.app as env


class Kernel:

    ### 初始化项目
    app = Flask(__name__, template_folder=env.TEMPLATE, static_folder=env.STATIC)
    app.debug = env.DEBUG

    #### 数据库实例化
    try:
        Database.init( app )
    except Exception as e:
        raise AttributeError( f'数据库链接异常：{e}' )

    #### 缓存驱动实例化


    #### 模块注入
    app.register_blueprint(bp)


    @classmethod
    def bootstrap(cls):
        return cls.app.run( host=env.HOST, port=env.PORT )



